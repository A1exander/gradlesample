package com.example.gradlesample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.xmpl.gradlesample.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}